import { getSpriteUrl, spritesType } from "entities/PokemonCard/api/getSpriteUrl";
import { Pokemon } from "pokenode-ts";
import React, { FC } from "react";
import { useQuery } from "react-query";
import { Card } from "shared/components/Card/Card";

interface PokemonCardProps {
  pokemon: Pokemon;
}

export const PokemonCard: FC<PokemonCardProps> = ({ pokemon }) => {
  return (
    <Card
      title={pokemon.name}
      imageSrc={
        getSpriteUrl(pokemon.id, spritesType.dreamWorld) || pokemon.sprites.front_default
      }
    ></Card>
  );
};
