export enum spritesType {
  dreamWorld = "other/dream-world",
  officialArtwork = "other/official-artwork",
  generationI = "versions/generation-i/yellow",
}
export const getSpriteUrl = (id: number, spriteType: spritesType): string | undefined => {
  // This condition is necessary because API does not have all the Pokemon sprite
  if (id < 500) {
    if (spriteType === spritesType.dreamWorld)
      return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${spriteType}/${id}.svg`;
    else {
      return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${spriteType}/${id}.png`;
    }
  }
  return undefined;
};
