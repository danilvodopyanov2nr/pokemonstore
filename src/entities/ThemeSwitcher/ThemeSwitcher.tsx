import { Dropdown } from "shared/components";
import { Theme } from "shared/contexts/themeContext/ThemeContext";
import { useTheme } from "shared/contexts/themeContext/useTheme";

export const ThemeSwitcher = () => {
  const { toggleTheme } = useTheme();

  const handleTheme = (themeName: keyof typeof Theme) => {
    toggleTheme(Theme[themeName]);
  };

  return (
    <Dropdown label="Сменить тему">
      <ul
        tabIndex={0}
        className="p-2 mt-4 shadow menu dropdown-content bg-base-100 rounded-box w-52"
      >
        <li onClick={() => handleTheme("DARK")}>
          <a>🌙 Dark</a>
        </li>
        <li onClick={() => handleTheme("LIGHT")}>
          <a>☀️ Light</a>
        </li>
        <li onClick={() => handleTheme("HALLOWEEN")}>
          <a>🎃 Halloween</a>
        </li>
      </ul>
    </Dropdown>
  );
};
