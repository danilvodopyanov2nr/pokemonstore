import { User } from "./../../types/user";
import React, { useState } from "react";
import { useMutation } from "react-query";

import { loginUserFn } from "shared/api/authApi";
import { LoginInput } from "shared/types/inputTypes";

export const useAuth = () => {
  const localUser = JSON.parse(localStorage.getItem("user"));
  const isAuth = !!localUser;
  const [user, setUser] = useState<User>(localUser);
  const {
    mutate: loginUser,
    isLoading,
    isError,
    error,
    isSuccess,
  } = useMutation((userData: LoginInput) => loginUserFn(userData), {
    onSuccess: (user) => {
      setUser(user);
      localStorage.setItem("user", JSON.stringify(user));
    },
  });
  return { loginUser, user, isAuth, isSuccess, isError, error };
};
