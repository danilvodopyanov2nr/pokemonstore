import { $api } from "shared/api";
import { LoginInput } from "shared/types/inputTypes";
import { User } from "shared/types/user";

export const loginUserFn = async (user: LoginInput) => {
  const response = await $api.post<User>("login", user);
  return response.data;
};
