import axios from "axios";
import { APIURL } from "shared/consts/apiUrl";

export const $api = axios.create({
  baseURL: APIURL,
  headers: {
    authorization: localStorage.getItem("user") || "",
  },
});
