import { useContext } from 'react';
import { Theme, ThemeContext, LOCAL_STORAGE_THEME_KEY } from './ThemeContext';

interface UseThemeResult {
  toggleTheme(theme: Theme): void;
  theme: Theme;
}

export const useTheme = (): UseThemeResult => {
  const context = useContext(ThemeContext);
  const { theme, setTheme } = context;

  if (!Object.keys(context).length) {
    throw new Error('useTheme must be used within a <ThemeContext />');
  }

  const toggleTheme = (themeProp: Theme) => {
    const newTheme = themeProp || theme;
    setTheme(newTheme);
    localStorage.setItem(LOCAL_STORAGE_THEME_KEY, newTheme);
  };

  return { theme, toggleTheme };
};
