import { Pokemon } from "pokenode-ts";
export type userRole = "USER" | "ADMIN";

export interface User {
  id: number;
  username: string;
  password: string;
  role: userRole;
  pokemons: Pokemon[];
}
