export type typeColors = "primary" | "secondary" | "accent" | "ghost" | "link";

export type typeSize = "lg" | "sm" | "xs";
