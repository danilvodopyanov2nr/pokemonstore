import { InputHTMLAttributes } from "react";

export type LoginInput = {
  username: string;
  password: string;
};

export interface InputProps
  extends Omit<InputHTMLAttributes<HTMLInputElement>, "id" | "className"> {
  name: string;
  label?: string;
}
