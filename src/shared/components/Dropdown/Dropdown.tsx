import React, { FC } from "react";

interface DropdownProps {
  label: string;
}

export const Dropdown: FC<DropdownProps> = ({ label, children }) => {
  return (
    <div className="dropdown dropdown-end">
      <label tabIndex={0} className="btn btn-ghost rounded-btn">
        {label}
      </label>
      {children}
    </div>
  );
};
