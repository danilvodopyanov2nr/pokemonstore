import React, { ButtonHTMLAttributes, FC } from "react";

import cn from "classnames";

import { typeColors, typeSize } from "shared/types/colorTypes";

interface ButtonProps {
  label: string;
  color?: typeColors;
  outline?: boolean;
  size?: typeSize;
  disabled?: boolean;
  additionalClassName?: string;
  onClick?(): void;
}

export const Button: FC<ButtonProps> = (props) => {
  const { label, color, outline, size, disabled, additionalClassName } = props;
  return (
    <div
      {...props}
      className={cn(
        "btn",
        {
          [`btn-${size}`]: size,
          [`btn-${color}`]: color,
          "btn-outline": outline,
          "btn-disabled": disabled,
        },
        additionalClassName
      )}
    >
      {label}
    </div>
  );
};
