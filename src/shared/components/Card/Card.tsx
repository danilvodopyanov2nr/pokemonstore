import React, { FC } from "react";

interface CardProps {
  imageSrc: string;
  title: string;
}

export const Card: FC<CardProps> = ({ imageSrc, title, children }) => {
  return (
    <div className="shadow-xl card card-side bg-base-100 w-max">
      <figure>
        <img src={imageSrc} alt="Movie" />
      </figure>
      <div className="card-body">
        <h2 className="card-title">{title}</h2>
        {children}
      </div>
    </div>
  );
};
