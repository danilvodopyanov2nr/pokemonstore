import React, { FC } from "react";
import { useFormContext, UseFormRegister } from "react-hook-form";
import { InputProps } from "shared/types/inputTypes";

export const Input: FC<InputProps> = ({ name, ...rest }): JSX.Element => {
  const methods = useFormContext();

  return (
    <input
      placeholder="Type here"
      className="w-full max-w-xs input"
      {...methods.register(name)}
      {...rest}
    />
  );
};
