import { useUsetStore } from "app/lib/store/useUserStore";
import { Form } from "pages/Login/components/Form";
import { useEffect } from "react";
import { SubmitHandler } from "react-hook-form";
import { useNavigate, useLocation } from "react-router-dom";
import { Input } from "shared/components/Input/Input";
import { useAuth } from "shared/lib/hooks/useAuth";
import { schema } from "webpack-dev-server";

interface FormFields {
  username: string;
  password: string;
}

interface LocationType {
  from: string;
}

export const LoginForm = (): JSX.Element => {
  const { loginUser, isAuth, user } = useAuth();
  const navigate = useNavigate();
  const { state } = useLocation();
  const addUser = useUsetStore((state) => state.addUser);

  const from = (state as LocationType)?.from || "/";
  console.log(location);
  const onSubmit = async ({ username, password }: FormFields) => {
    console.log(username);
    loginUser({
      username,
      password,
    });
  };

  useEffect(() => {
    addUser(user);
  }, [user]);

  useEffect(() => {
    if (isAuth) navigate(from);
  }, [isAuth]);

  return (
    <Form<FormFields> onSubmit={onSubmit} className="space-y-6">
      <Input name="username" label="Username" />
      <Input name="password" type="password" label="Password" />
      <div>
        <button
          type="submit"
          className="flex justify-center w-full px-4 py-2 btn btn-indigo"
        >
          Sign in
        </button>
      </div>
    </Form>
  );
};
