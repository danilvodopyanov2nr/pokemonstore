import { useUsetStore } from "app/lib/store/useUserStore";
import React, { useEffect, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { Button } from "shared/components/Button/Button";
import { useAuth } from "shared/lib/hooks/useAuth";
import { useForm, SubmitHandler } from "react-hook-form";
import { Input } from "shared/components/Input/Input";
import { LoginForm } from "pages/Login/components/LoginForm";

export const Login = () => {
  return <LoginForm />;
};
