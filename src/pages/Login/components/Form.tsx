import React, { FormHTMLAttributes, ReactElement, ReactNode, useState } from "react";
import { FormProvider, SubmitHandler, useForm, UseFormProps } from "react-hook-form";
import { InputProps } from "shared/types/inputTypes";

interface FormProps<T> extends Omit<FormHTMLAttributes<HTMLFormElement>, "onSubmit"> {
  form?: UseFormProps<T>;
  children: ReactElement | ReactElement[];
  onSubmit: SubmitHandler<T>;
}

type EnrichedChildren<T> = InputProps;

export const Form = <T extends object>({
  children,
  onSubmit,
  form,
  ...rest
}: FormProps<T>): JSX.Element => {
  const methods = useForm<T>(form);

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        {React.Children.map(children, (child: ReactNode) => {
          if (!React.isValidElement<EnrichedChildren<typeof child>>(child)) {
            return child;
          }

          return React.createElement(child.type, {
            ...{
              ...child.props,
              register: methods.register,
              key: child.props.name,
            },
          });
        })}
      </form>
    </FormProvider>
  );
};
