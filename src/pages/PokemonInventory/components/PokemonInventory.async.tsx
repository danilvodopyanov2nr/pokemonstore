import { lazy } from "react";

export const PokemonInventoryAsync = lazy(() => import("./PokemonInventory"));
