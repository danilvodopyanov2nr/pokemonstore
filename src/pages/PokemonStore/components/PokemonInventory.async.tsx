import { lazy } from "react";

export const PokemonStoreAsync = lazy(() => import("./PokemonStore"));
