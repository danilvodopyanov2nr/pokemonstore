import { User } from "shared/types/user";
import { create } from "zustand";

interface UserState {
  user: Partial<User>;
  addUser: (user: User) => void;
}
export const useUsetStore = create<UserState>((set) => ({
  user: {},
  addUser: (user) => set((state) => ({ user: user })),
}));
