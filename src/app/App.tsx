import { ThemeSwitcher } from "entities";
import { PokemonCard } from "entities/PokemonCard/PokemonCard";
import { Login } from "pages/Login/components/Login";
import { PokemonInventory } from "pages/PokemonInventory";
import { PokemonStore } from "pages/PokemonStore";
import { Pokemon } from "pokenode-ts";
import { Suspense, useEffect } from "react";
import { QueryClient, useQuery } from "react-query";
import { Link, Routes, Route, useLocation } from "react-router-dom";
import { $api } from "shared/api";
import { Button } from "shared/components/Button/Button";
import { useAuth } from "shared/lib/hooks/useAuth";

import "./styles.css";

export const App = () => {
  const { data, isSuccess } = useQuery<Pokemon>("pokemon", () =>
    fetch("https://pokeapi.co/api/v2/pokemon/800").then((res) => res.json())
  );
  const location = useLocation();
  // const { data: posts } = useQuery(
  //   "posts",
  //   () => $api.get("posts").then((res) => res.data),
  //   {
  //     retry: true,
  //   }
  // );

  // console.log(posts);

  if (!isSuccess) return <p>"Loading..."</p>;

  return (
    <div className="app background-dotted">
      <div className="navbar bg-base-300 rounded-box">
        <div className="flex-1 px-2 lg:flex-none">
          <a className="text-lg font-bold">daisyUI</a>
        </div>
        <div className="flex justify-end flex-1 px-2">
          <div className="flex items-stretch">
            <Link
              to={{ pathname: "/pokemoninventory" }}
              state={{ from: location.pathname }}
            >
              <Button label="Pokemon Inventory" color="accent" outline size="sm" />
            </Link>
            <Link to="/" state={{ from: location.pathname }}>
              <Button label="Pokemon store" color="accent" outline size="sm" />
            </Link>
            <Link to="/login" state={{ from: location.pathname }}>
              <Button label="Login" color="accent" outline size="sm" />
            </Link>
            <ThemeSwitcher />
          </div>
        </div>
      </div>

      <Suspense fallback={<div>Loading...</div>}>
        <Routes>
          <Route path={"/pokemoninventory"} element={<PokemonInventory />} />
          <Route path={"/login"} element={<Login />} />
          <Route path={"/"} element={<PokemonStore />} />
        </Routes>
      </Suspense>
      <PokemonCard pokemon={data}></PokemonCard>
    </div>
  );
};
