/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./build/*.html'],
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {},
  },
  plugins: [require('daisyui')],
  daisyui: {
    themes: ['dark', 'light', 'halloween'],
  },
};
